# vcloud-cli

## Purpose

Provide an interface to the VCloud director OpenAPI to manage rules, port profiles and ipsets. 

The API documentation from VMWare: 
https://developer.vmware.com/docs/vmware-cloud-director/latest/index.html

## configure-vdc

The command `configure-vdc` configures port profiles, ipsets and rules in a VDC. The command reads a YAML file (see example in examples directory) for the configuration of the VDC. The YAML is the desired state for this VDC, and the tool will issue Inserts, Updates and Deletes as required. An important feature is the Rule Prefix. Only rules, ipSets and port profiles which name starts with this prefix will be managed. All other rules stay unchanged. 

The authentication supports either basic auth or JWT token. 

Basic auth can be configured in the following environment variables: 
- VCLOUD_USERNAME: username in VCloud Director
- VCLOUD_PASSWORD: password in VCloud Director

Alternatively, configure the following JWT tokens in the environment: 
- AZ1_TOKEN: JWT token for VCloud Director in AZ1 
- AZ2_TOKEN: JWT token for VCloud Director in AZ2 

How to run: 
```
vcloud-cli configure-vdc
        --config=examples/test.config.yaml
        --ipset-config=examples/ipsets.yaml
        --tenant=MY-TENANT 
        --az1-vcloud-api-url=<cloud director az1 api>
        --az2-vcloud-api-url=<cloud director az2 api>
        --rule-prefix=MYRULES-
        --dry-run=true
```     
The `dry-run` flag will make sure the tool does not commit any changes. Remove this flag if you are confident. 


## print-tenant 

The command `print-tenant` will read port profiles, ipsets and rules from all VDCs in the specified tenant. For each VDC a YAML file is generated which could beused as input for the command `configure-vdc`. Also, diff files are created to compare ipsets and rules in a tenant. 

The authentication environment variables are the same as for `configure-vdc`. 

How to run: 
```
vcloud-cli print-tenant 
        --tenant=MY-TENANT 
        --az1-vcloud-api-url=<cloud director az1 api>
        --az2-vcloud-api-url=<cloud director az2 api>
        --rule-prefix=MYRULES-
```    


