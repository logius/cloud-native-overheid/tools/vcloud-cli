package vdc

import (
	"fmt"
	"net/url"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
)

type VDCGroup struct {
	ID          string
	Name        string
	Org         *govcd.Org
	EdgeGateway *govcd.NsxtEdgeGateway
}

func getVDCGroup(vcdClient *govcd.VCDClient, org *govcd.Org, vdc *govcd.Vdc) (*VDCGroup, error) {

	urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(types.OpenApiPathVersion1_0_0 + "vdcGroups")
	if err != nil {
		return nil, err
	}

	queryParams := url.Values{}
	queryParams.Add("filter", "participatingOrgVdcs.vdcRef.id=="+url.PathEscape(vdc.Vdc.ID))

	vdcGroups := []VDCGroup{}
	err = vcdClient.Client.OpenApiGetAllItems(vcdClient.Client.APIVersion, urlRef, queryParams, &vdcGroups, nil)
	if err != nil {
		return nil, err
	}

	if len(vdcGroups) > 0 {
		vdcGroup := &vdcGroups[0]
		vdcGroup.Org = org

		edgeGateway, err := vdcGroup.getGateway()
		if err != nil {
			return nil, err
		}

		vdcGroup.EdgeGateway = edgeGateway

		return vdcGroup, nil
	}
	return nil, fmt.Errorf("could not find datacenter group for %q", vdc.Vdc.Name)
}

func (vdcGroup *VDCGroup) getGateway() (*govcd.NsxtEdgeGateway, error) {

	queryParams := url.Values{}
	queryParams.Add("filter", "ownerRef.name=="+url.PathEscape(vdcGroup.Name))

	edgeGatewayList, err := vdcGroup.Org.GetAllNsxtEdgeGateways(queryParams)
	if err != nil {
		return nil, err
	}

	if len(edgeGatewayList) > 0 {
		return edgeGatewayList[0], nil
	}
	return nil, fmt.Errorf("could not find edgegateway for %q", vdcGroup.Name)
}
