package vdc

import (
	"fmt"
	"log"
	"net/url"
	"reflect"
	"sort"
	"strings"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

func (vdcGroup *VDCGroup) manageApplicationPortconfig(vcdClient *govcd.VCDClient, vdcConfig *config.VDCConfig, rulePrefix string) error {
	existingAppPorts, err := vdcGroup.getApplicationPorts(vcdClient)
	if err != nil {
		return err
	}

	for _, appPortConfig := range vdcConfig.ApplicationPorts {
		existingAppPort := getAppPortConfig(existingAppPorts, appPortConfig.Name)

		if existingAppPort != nil {
			if err := vdcGroup.handleExistingAppPortConfig(existingAppPort, &appPortConfig); err != nil {
				return err
			}
		} else {
			log.Printf("Create Application Port Profile %q in VDC %q", appPortConfig.Name, vdcConfig.Name)

			appPortProfile := types.NsxtAppPortProfile{
				Name:             appPortConfig.Name,
				Description:      appPortConfig.Description,
				ApplicationPorts: toNsxtAppPortProfilePorts(appPortConfig.Ports),
				OrgRef: &types.OpenApiReference{
					ID: vdcGroup.Org.Org.ID,
				},
				ContextEntityId: vdcGroup.ID,
				Scope:           types.ApplicationPortProfileScopeTenant,
			}

			if dryRun {
				continue
			}
			_, err := vdcGroup.Org.CreateNsxtAppPortProfile(&appPortProfile)
			if err != nil {
				return err
			}
		}
	}

	if err := vdcGroup.removeObsoleteAppPorts(existingAppPorts, vdcConfig.ApplicationPorts, rulePrefix); err != nil {
		return err
	}

	return nil
}

func (vdcGroup *VDCGroup) handleExistingAppPortConfig(existingPortConfig *govcd.NsxtAppPortProfile, appPortConfig *config.ApplicationPortProfileConfig) error {
	var different = false

	ports := toNsxtAppPortProfilePorts(appPortConfig.Ports)
	if !appPortConfigsEqual(existingPortConfig.NsxtAppPortProfile.ApplicationPorts, ports) {
		existingPortConfig.NsxtAppPortProfile.ApplicationPorts = ports
		log.Printf("Application Port Profile %q exists but has different ports configured", appPortConfig.Name)
		different = true
	}
	if existingPortConfig.NsxtAppPortProfile.Description != appPortConfig.Description {
		log.Printf("Application Port Profile %q exists but has different description", appPortConfig.Name)
		existingPortConfig.NsxtAppPortProfile.Description = appPortConfig.Description
		different = true
	}

	if different {

		if dryRun {
			return nil
		}
		_, err := existingPortConfig.Update(existingPortConfig.NsxtAppPortProfile)
		if err != nil {
			return err
		}
	}
	return nil
}

func appPortConfigsEqual(ports1 []types.NsxtAppPortProfilePort, ports2 []types.NsxtAppPortProfilePort) bool {
	if len(ports1) != len(ports2) {
		return false
	}
	for n := range ports1 {
		if ports1[n].Protocol != ports2[n].Protocol {
			return false
		}
		sort.Strings(ports1[n].DestinationPorts)
		sort.Strings(ports2[n].DestinationPorts)
		if !reflect.DeepEqual(ports1[n].DestinationPorts, ports2[n].DestinationPorts) {
			return false
		}
	}
	return true
}

func toNsxtAppPortProfilePorts(appPortsConfig []config.ApplicationPortConfig) []types.NsxtAppPortProfilePort {
	appPorts := make([]types.NsxtAppPortProfilePort, 0)
	for _, appPortsConfig := range appPortsConfig {
		appPort := types.NsxtAppPortProfilePort{
			Protocol:         appPortsConfig.Protocol,
			DestinationPorts: appPortsConfig.DestinationPorts,
		}
		sort.Strings(appPort.DestinationPorts)
		appPorts = append(appPorts, appPort)
	}
	return appPorts
}

func (vdcGroup *VDCGroup) getApplicationPorts(vcdClient *govcd.VCDClient) ([]*govcd.NsxtAppPortProfile, error) {

	queryParams := url.Values{}
	queryParams.Add("filter", "_context=="+url.PathEscape(vdcGroup.ID))
	queryParams.Add("sortAsc", "name")

	applications, err := vdcGroup.Org.GetAllNsxtAppPortProfiles(queryParams, types.ApplicationPortProfileScopeTenant)
	if err != nil {
		return nil, err
	}
	return applications, nil
}

func getAppPortConfig(appPorts []*govcd.NsxtAppPortProfile, appPortName string) *govcd.NsxtAppPortProfile {
	for _, appPort := range appPorts {
		if appPort.NsxtAppPortProfile.Name == appPortName {
			return appPort
		}
	}
	return nil
}

func toPortConfig(applicationPorts []types.NsxtAppPortProfilePort) []config.ApplicationPortConfig {
	appPortConfigList := make([]config.ApplicationPortConfig, 0)

	for _, portConfig := range applicationPorts {
		appPortConfig := config.ApplicationPortConfig{
			Protocol:         portConfig.Protocol,
			DestinationPorts: portConfig.DestinationPorts,
		}
		sort.Strings(appPortConfig.DestinationPorts)
		appPortConfigList = append(appPortConfigList, appPortConfig)

	}
	return appPortConfigList
}

func (vdcGroup *VDCGroup) removeObsoleteAppPorts(applicationPorts []*govcd.NsxtAppPortProfile, appConfigList []config.ApplicationPortProfileConfig, prefix string) error {

	for _, appPort := range applicationPorts {
		if strings.HasPrefix(appPort.NsxtAppPortProfile.Name, prefix) && !isAppProfileDefined(appConfigList, appPort.NsxtAppPortProfile.Name) {

			if dryRun {
				continue
			}

			// Port profiles might be in use in other VDCs in the same tenant. Is it quite cumbersome to detect this, as we would need to fetch rules from all VDCs.
			// Instead, we just try a delete and check the error.
			err := appPort.Delete()
			if err != nil {
				// if the error message indicates the port profile is still in use, we can ignore the error.
				if strings.Contains(err.Error(), "cannot be deleted as either it has children or it is being referenced by other objects") {
					continue
				}
				return fmt.Errorf("could not delete Application Port Profile %q", appPort.NsxtAppPortProfile.Name)
			}
			log.Printf("Remove Application Port Profile %q", appPort.NsxtAppPortProfile.Name)
		}
	}
	return nil
}

func isAppProfileDefined(appConfigList []config.ApplicationPortProfileConfig, name string) bool {
	for _, appConfig := range appConfigList {
		if appConfig.Name == name {
			return true
		}
	}
	return false
}
