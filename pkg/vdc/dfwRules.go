package vdc

import (
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

const distributedFirewallRulesEndPointPath = types.OpenApiPathVersion1_0_0 + "vdcGroups/%s/dfwPolicies/default/rules"

func (vdcGroup *VDCGroup) dfwPolicies(vcdClient *govcd.VCDClient, vdcConfig *config.VDCConfig, rulePrefix string) error {

	fwRules, err := vdcGroup.retrieveDfwPolicies(vcdClient)
	if err != nil {
		return err
	}

	fwRules, err = vdcGroup.removeObsoleteDFWRules(vcdClient, vdcConfig.DistributedFirewall.Rules, fwRules, rulePrefix)
	if err != nil {
		return err
	}

	fwRules, changed, err := vdcGroup.configureFirewallRules(vdcConfig.DistributedFirewall.Rules, fwRules, rulePrefix)
	if err != nil {
		return err
	}

	if changed {

		type DfwRules struct {
			Values []types.NsxtFirewallRule `json:"values"`
		}

		dfwRules := DfwRules{
			Values: fwRules,
		}

		log.Printf("Update Distributed Firewall ruleset in VDC %q", vdcGroup.Name)
		if !dryRun {
			var outType interface{}
			rulesURL := fmt.Sprintf(distributedFirewallRulesEndPointPath, url.PathEscape(vdcGroup.ID))
			urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
			if err != nil {
				return err
			}
			err = vcdClient.Client.OpenApiPutItem(vcdClient.Client.APIVersion, urlRef, url.Values{}, dfwRules, &outType, nil)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (vdcGroup *VDCGroup) retrieveDfwPolicies(vcdClient *govcd.VCDClient) ([]types.NsxtFirewallRule, error) {
	rulesURL := fmt.Sprintf(distributedFirewallRulesEndPointPath, url.PathEscape(vdcGroup.ID))
	urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
	if err != nil {
		return nil, err
	}
	queryParams := url.Values{}
	fwRules := []types.NsxtFirewallRule{}
	err = vcdClient.Client.OpenApiGetAllItems(vcdClient.Client.APIVersion, urlRef, queryParams, &fwRules, nil)
	if err != nil {
		return nil, err
	}
	return fwRules, nil
}

func (vdcGroup *VDCGroup) removeObsoleteDFWRules(vcdClient *govcd.VCDClient, rulesConfig []config.FirewallRuleConfig, fwRules []types.NsxtFirewallRule, prefix string) ([]types.NsxtFirewallRule, error) {

	rulesNotRemoved := make([]types.NsxtFirewallRule, 0)

	for _, fwRule := range fwRules {
		if strings.HasPrefix(fwRule.Name, prefix) && !isFirewallRuleDefined(rulesConfig, fwRule) {
			log.Printf("Remove Distributed Firewall rule %q", fwRule.Name)
			if dryRun {
				continue
			}

			rulesURL := fmt.Sprintf(distributedFirewallRulesEndPointPath+"/%s", url.PathEscape(vdcGroup.ID), url.PathEscape(fwRule.ID))
			urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
			if err != nil {
				return nil, err
			}
			queryParams := url.Values{}
			err = vcdClient.Client.OpenApiDeleteItem(vcdClient.Client.APIVersion, urlRef, queryParams, nil)
			if err != nil {
				return nil, err
			}
		} else {
			rulesNotRemoved = append(rulesNotRemoved, fwRule)
		}
	}
	return rulesNotRemoved, nil
}
