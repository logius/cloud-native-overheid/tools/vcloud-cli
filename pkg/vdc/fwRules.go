package vdc

import (
	"fmt"
	"log"
	"net/url"

	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

func (vdcGroup *VDCGroup) configureFirewallRules(rulesConfig []config.FirewallRuleConfig, fwRules []types.NsxtFirewallRule, rulePrefix string) ([]types.NsxtFirewallRule, bool, error) {

	changed := false

	for _, fwRuleConfig := range rulesConfig {
		fwRule := getRule(fwRules, fwRuleConfig.Name)

		if fwRule == nil {
			changed = true
			fwRule, err := vdcGroup.createFWRule(&fwRuleConfig)
			if err != nil {
				return nil, changed, err
			}
			fwRules = append(fwRules, *fwRule)
		} else {
			ruleIsUpdated, err := vdcGroup.updateFWRule(&fwRuleConfig, fwRule)
			if err != nil {
				return nil, changed, err
			}
			if ruleIsUpdated {
				changed = true
			}
		}
	}

	fwRules = sortRules(fwRules, rulesConfig)

	return fwRules, changed, nil
}

func sortRules(fwRules []types.NsxtFirewallRule, rulesConfig []config.FirewallRuleConfig) []types.NsxtFirewallRule {
	sortedRules := make([]types.NsxtFirewallRule, 0)

	// add rules in the sort order as specified in the config
	for _, fwRuleConfig := range rulesConfig {
		fwRule := getRule(fwRules, fwRuleConfig.Name)
		if fwRule != nil {
			sortedRules = append(sortedRules, *fwRule)
		}
	}

	// add remaining rules from the original list that are not in the sortedlist yet
	for _, fwRule := range fwRules {
		if getRule(sortedRules, fwRule.Name) == nil {
			sortedRules = append(sortedRules, fwRule)
		}
	}

	return sortedRules
}

func (vdcGroup *VDCGroup) createFWRule(fwRuleConfig *config.FirewallRuleConfig) (*types.NsxtFirewallRule, error) {
	log.Printf("Create Firewall rule %q", fwRuleConfig.Name)

	applications, err := vdcGroup.setApplicationIds(toOpenAPIReference(fwRuleConfig.Application))
	if err != nil {
		return nil, err
	}
	sourceRef, err := vdcGroup.setFWGroupIds(toOpenAPIReference(fwRuleConfig.Source))
	if err != nil {
		return nil, err
	}
	destRef, err := vdcGroup.setFWGroupIds(toOpenAPIReference(fwRuleConfig.Destination))
	if err != nil {
		return nil, err
	}

	fwRule := types.NsxtFirewallRule{
		Name:                      fwRuleConfig.Name,
		SourceFirewallGroups:      sourceRef,
		DestinationFirewallGroups: destRef,
		Action:                    firewallRuleAction(fwRuleConfig),
		Enabled:                   true,
		IpProtocol:                "IPV4_IPV6",
		ApplicationPortProfiles:   applications,
	}

	return &fwRule, nil
}

// check for changes, prepare the FW object and return true if an update should be performed
func (vdcGroup *VDCGroup) updateFWRule(fwRuleConfig *config.FirewallRuleConfig, fwRule *types.NsxtFirewallRule) (bool, error) {
	changed := false

	// the openapi references are initially created with only the name
	// only when the update should be performed we fetch the IDs
	sourceRef := toOpenAPIReference(fwRuleConfig.Source)
	destRef := toOpenAPIReference(fwRuleConfig.Destination)
	applications := toOpenAPIReference(fwRuleConfig.Application)

	if !fwRule.Enabled {
		changed = true
		log.Printf("Enable Firewall rule %q", fwRuleConfig.Name)
		fwRule.Enabled = true
	}
	if fwRule.Action != firewallRuleAction(fwRuleConfig) {
		changed = true
		log.Printf("Update action in Firewall rule %q", fwRuleConfig.Name)
		fwRule.Action = firewallRuleAction(fwRuleConfig)
	}
	if !openAPIReferencesEqual(fwRule.SourceFirewallGroups, sourceRef) {
		changed = true
		log.Printf("Update source firewall group in Firewall rule %q", fwRuleConfig.Name)
		sourceRef, err := vdcGroup.setFWGroupIds(sourceRef)
		if err != nil {
			return changed, err
		}
		fwRule.SourceFirewallGroups = sourceRef
	}
	if !openAPIReferencesEqual(fwRule.DestinationFirewallGroups, destRef) {
		changed = true
		log.Printf("Update destination firewall group in Firewall rule %q", fwRuleConfig.Name)
		destRef, err := vdcGroup.setFWGroupIds(destRef)
		if err != nil {
			return changed, err
		}
		fwRule.DestinationFirewallGroups = destRef
	}
	if !openAPIReferencesEqual(fwRule.ApplicationPortProfiles, applications) {
		changed = true
		log.Printf("Update applications in Firewall rule %q", fwRuleConfig.Name)
		applications, err := vdcGroup.setApplicationIds(applications)
		if err != nil {
			return changed, err
		}
		fwRule.ApplicationPortProfiles = applications
	}

	return changed, nil
}

func isFirewallRuleDefined(rulesConfig []config.FirewallRuleConfig, fwRule types.NsxtFirewallRule) bool {
	for _, fwRuleConfig := range rulesConfig {
		if fwRuleConfig.Name == fwRule.Name {
			return true
		}
	}
	return false
}

func getRule(fwRules []types.NsxtFirewallRule, ruleName string) *types.NsxtFirewallRule {
	for n, fwRule := range fwRules {
		if fwRule.Name == ruleName {
			return &fwRules[n]
		}
	}
	return nil
}

func (vdcGroup *VDCGroup) setFWGroupIds(references []types.OpenApiReference) ([]types.OpenApiReference, error) {
	for n, ref := range references {
		queryParams := url.Values{}
		queryParams.Add("filter", "ownerRef.id=="+url.PathEscape(vdcGroup.ID)+";name=="+ref.Name) // ";" is the Boolean AND operator in FIQL
		groups, err := vdcGroup.Org.GetAllNsxtFirewallGroups(queryParams, "")
		if err != nil {
			return references, err
		}
		if len(groups) != 1 {
			return references, fmt.Errorf("expected exactly one firewall group %q in vdc %q. Got %d", ref.Name, vdcGroup.Name, len(groups))
		}
		references[n].ID = groups[0].NsxtFirewallGroup.ID
	}
	return references, nil
}

func (vdcGroup *VDCGroup) setApplicationIds(references []types.OpenApiReference) ([]types.OpenApiReference, error) {
	for n, ref := range references {

		queryParams := url.Values{}
		queryParams.Add("filter", "_context=="+url.PathEscape(vdcGroup.ID)+";name=="+ref.Name) // ";" is the Boolean AND operator in FIQL

		applications, err := vdcGroup.Org.GetAllNsxtAppPortProfiles(queryParams, "")

		if len(applications) != 1 {
			return nil, fmt.Errorf("expected exactly one Application Port Profile with name %q. Got %d", ref.Name, len(applications))
		}

		if err != nil {
			return references, err
		}
		references[n].ID = applications[0].NsxtAppPortProfile.ID
	}
	return references, nil
}

func toOpenAPIReference(list []string) []types.OpenApiReference {

	refs := make([]types.OpenApiReference, 0)
	for _, item := range list {
		ref := types.OpenApiReference{
			Name: item,
		}
		refs = append(refs, ref)
	}
	return refs
}

func firewallRuleAction(fwRuleConfig *config.FirewallRuleConfig) string {
	if fwRuleConfig.Action == "" {
		return "ALLOW"
	}
	return fwRuleConfig.Action
}

func openAPIReferencesEqual(refs1 []types.OpenApiReference, refs2 []types.OpenApiReference) bool {
	if len(refs1) != len(refs2) {
		return false
	}
	for _, ref1 := range refs1 {
		if !refExists(refs2, ref1) {
			return false
		}
	}
	for _, ref2 := range refs2 {
		if !refExists(refs1, ref2) {
			return false
		}
	}
	return true
}

func refExists(refs []types.OpenApiReference, refToCheck types.OpenApiReference) bool {
	for _, ref := range refs {
		if ref.Name == refToCheck.Name {
			return true
		}
	}
	return false
}
