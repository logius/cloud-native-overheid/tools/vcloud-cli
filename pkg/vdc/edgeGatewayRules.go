package vdc

import (
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

// EdgeGatewayRules is a direct mapping to the JSON messages in the VCloud OpenAPI
type EdgeGatewayRules struct {
	SystemRules      []types.NsxtFirewallRule `json:"systemRules"`
	UserDefinedRules []types.NsxtFirewallRule `json:"userDefinedRules"`
	DefaultRules     []types.NsxtFirewallRule `json:"defaultRules"`
}

const edgeGatewayFirewallRulesEndPointPath = types.OpenApiPathVersion1_0_0 + types.OpenApiEndpointNsxtFirewallRules

func (vdcGroup *VDCGroup) manageEdgeGatewayRules(vcdClient *govcd.VCDClient, vdcConfig *config.VDCConfig, rulePrefix string) error {

	fwRules, err := vdcGroup.retrieveEdgeGatewayRules(vcdClient)
	if err != nil {
		return err
	}

	updatedUserDefinedRules, err := vdcGroup.removeObsoleteEdgeGatewayRules(vcdClient, vdcConfig.EdgeGatewayFirewall.Rules, fwRules.UserDefinedRules, rulePrefix)
	if err != nil {
		return err
	}

	updatedUserDefinedRules, changed, err := vdcGroup.configureFirewallRules(vdcConfig.EdgeGatewayFirewall.Rules, updatedUserDefinedRules, rulePrefix)
	if err != nil {
		return err
	}

	if changed {

		fwRules.UserDefinedRules = updatedUserDefinedRules

		log.Printf("Update EdgeGateway Firewall ruleset in VDC %q", vdcGroup.Name)
		if !dryRun {
			var outType interface{}

			rulesURL := fmt.Sprintf(edgeGatewayFirewallRulesEndPointPath, url.PathEscape(vdcGroup.EdgeGateway.EdgeGateway.ID))
			urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
			if err != nil {
				return err
			}
			err = vcdClient.Client.OpenApiPutItem(vcdClient.Client.APIVersion, urlRef, url.Values{}, fwRules, &outType, nil)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (vdcGroup *VDCGroup) removeObsoleteEdgeGatewayRules(vcdClient *govcd.VCDClient, rulesConfig []config.FirewallRuleConfig, fwRules []types.NsxtFirewallRule, prefix string) ([]types.NsxtFirewallRule, error) {

	rulesNotRemoved := make([]types.NsxtFirewallRule, 0)

	for _, fwRule := range fwRules {
		if strings.HasPrefix(fwRule.Name, prefix) && !isFirewallRuleDefined(rulesConfig, fwRule) {
			log.Printf("Remove EdgeGateway Firewall rule %q", fwRule.Name)
			if dryRun {
				continue
			}

			rulesURL := fmt.Sprintf(edgeGatewayFirewallRulesEndPointPath+"/%s", url.PathEscape(vdcGroup.EdgeGateway.EdgeGateway.ID), url.PathEscape(fwRule.ID))
			urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
			if err != nil {
				return nil, err
			}

			err = vcdClient.Client.OpenApiDeleteItem(vcdClient.Client.APIVersion, urlRef, url.Values{}, nil)
			if err != nil {
				return nil, err
			}
		} else {
			rulesNotRemoved = append(rulesNotRemoved, fwRule)
		}
	}
	return rulesNotRemoved, nil
}

func (vdcGroup *VDCGroup) retrieveEdgeGatewayRules(vcdClient *govcd.VCDClient) (*EdgeGatewayRules, error) {
	rulesURL := fmt.Sprintf(edgeGatewayFirewallRulesEndPointPath, url.PathEscape(vdcGroup.EdgeGateway.EdgeGateway.ID))
	urlRef, err := vcdClient.Client.OpenApiBuildEndpoint(rulesURL)
	if err != nil {
		return nil, err
	}

	fwRules := EdgeGatewayRules{}

	err = vcdClient.Client.OpenApiGetItem(vcdClient.Client.APIVersion, urlRef, url.Values{}, &fwRules, nil)
	if err != nil {
		return nil, err
	}
	return &fwRules, nil
}
