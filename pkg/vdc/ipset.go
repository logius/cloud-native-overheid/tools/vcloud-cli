package vdc

import (
	"log"
	"net/url"
	"reflect"
	"sort"
	"strings"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

func (vdcGroup *VDCGroup) manageIPSets(vdcConfig *config.VDCConfig, rulePrefix string) error {

	queryParams := url.Values{}
	queryParams.Add("filter", "ownerRef.id=="+url.PathEscape(vdcGroup.ID))
	queryParams.Add("sortAsc", "name")

	ipSets, err := vdcGroup.Org.GetAllNsxtFirewallGroups(queryParams, types.FirewallGroupTypeIpSet)
	if err != nil {
		return err
	}

	for _, ipSetConfig := range vdcConfig.IPSets {
		existingIPSet := getIPSet(ipSets, ipSetConfig.Name)

		if existingIPSet != nil {
			vdcGroup.handleExistingIPSet(existingIPSet, &ipSetConfig)
		} else {
			log.Printf("Create IPSet %q in VDC %q", ipSetConfig.Name, vdcConfig.Name)

			group := types.NsxtFirewallGroup{
				Name: ipSetConfig.Name,
				OwnerRef: &types.OpenApiReference{
					Name: vdcGroup.EdgeGateway.EdgeGateway.Name,
					ID:   vdcGroup.EdgeGateway.EdgeGateway.ID,
				},
				Description: ipSetConfig.Description,
				IpAddresses: ipSetConfig.Addresses,
			}

			if dryRun {
				continue
			}
			_, err := vdcGroup.EdgeGateway.CreateNsxtFirewallGroup(&group)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func getIPSet(ipSets []*govcd.NsxtFirewallGroup, ipSetName string) *govcd.NsxtFirewallGroup {
	for _, ipSet := range ipSets {
		if ipSet.NsxtFirewallGroup.Name == ipSetName {
			return ipSet
		}
	}
	return nil
}

func (vdcGroup *VDCGroup) handleExistingIPSet(existingIPSet *govcd.NsxtFirewallGroup, ipSetConfig *config.IPSet) error {
	var different = false

	// Get the details because GetAllNsxtFirewallGroups() does not fetch the details of the IP Adresses
	existingIPSetDetails, err := vdcGroup.Org.GetNsxtFirewallGroupById(existingIPSet.NsxtFirewallGroup.ID)
	if err != nil {
		return err
	}
	sort.Strings(existingIPSetDetails.NsxtFirewallGroup.IpAddresses)
	sort.Strings(ipSetConfig.Addresses)

	if !reflect.DeepEqual(existingIPSetDetails.NsxtFirewallGroup.IpAddresses, ipSetConfig.Addresses) {
		log.Printf("IPSet %q exists but has different IP Adresses: %s - %s", ipSetConfig.Name, existingIPSetDetails.NsxtFirewallGroup.IpAddresses, ipSetConfig.Addresses)
		different = true
	}
	if existingIPSetDetails.NsxtFirewallGroup.Description != ipSetConfig.Description {
		log.Printf("IPSet %q exists but has different description", ipSetConfig.Name)
		different = true
	}

	if different {

		group := types.NsxtFirewallGroup{
			ID:   existingIPSetDetails.NsxtFirewallGroup.ID,
			Name: ipSetConfig.Name,
			OwnerRef: &types.OpenApiReference{
				Name: vdcGroup.EdgeGateway.EdgeGateway.Name,
				ID:   vdcGroup.EdgeGateway.EdgeGateway.ID,
			},
			Description: ipSetConfig.Description,
			IpAddresses: ipSetConfig.Addresses,
		}
		if dryRun {
			return nil
		}
		_, err := existingIPSetDetails.Update(&group)
		if err != nil {
			return err
		}
	}
	return nil
}

func (vdcGroup *VDCGroup) removeObsoleteIPSets(ipSetConfig []config.IPSet, prefix string) error {

	queryParams := url.Values{}
	queryParams.Add("filter", "ownerRef.id=="+url.PathEscape(vdcGroup.ID))
	queryParams.Add("sortAsc", "name")
	ipSets, err := vdcGroup.Org.GetAllNsxtFirewallGroups(queryParams, types.FirewallGroupTypeIpSet)
	if err != nil {
		return err
	}
	for _, ipSet := range ipSets {
		if strings.HasPrefix(ipSet.NsxtFirewallGroup.Name, prefix) && !isIPSetDefined(ipSetConfig, ipSet) {
			log.Printf("Remove IPSet %q", ipSet.NsxtFirewallGroup.Name)
			if dryRun {
				continue
			}

			err := ipSet.Delete()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func isIPSetDefined(ipSetConfigList []config.IPSet, ipSet *govcd.NsxtFirewallGroup) bool {
	for _, ipSetConfig := range ipSetConfigList {
		if ipSetConfig.Name == ipSet.NsxtFirewallGroup.Name {
			return true
		}
	}
	return false
}
