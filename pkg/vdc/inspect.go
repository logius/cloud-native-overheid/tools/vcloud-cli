package vdc

import (
	"fmt"
	"net/url"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"github.com/vmware/go-vcloud-director/v2/types/v56"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/vcloud"
)

func GetRulesForTenant(tenant string, apiURL string, zone string, token string, rulePrefix string) ([]config.ConfigFile, error) {
	vcdClient, err := vcloud.CreateClient(tenant, apiURL, token)
	if err != nil {
		return nil, err
	}

	org, err := vcdClient.GetOrgByNameOrId(tenant)
	if err != nil {
		return nil, fmt.Errorf("organization %s not found: %w", tenant, err)
	}

	vdcList, err := org.QueryOrgVdcList()
	if err != nil {
		return nil, err
	}

	vdcConfigFiles := make([]config.ConfigFile, 0)
	for _, vdcRecord := range vdcList {

		vdcOrg, err := org.GetVDCByName(vdcRecord.Name, false)
		if err != nil {
			return nil, err
		}
		vdcGroup, err := getVDCGroup(vcdClient, org, vdcOrg)
		if err != nil {
			return nil, err
		}

		vdcConfigFile := vdcGroup.getRules(vcdClient)
		vdcConfigFile.VDC.Zone = zone
		vdcConfigFiles = append(vdcConfigFiles, vdcConfigFile)
	}

	return vdcConfigFiles, nil

}

func (vdcGroup *VDCGroup) getRules(vcdClient *govcd.VCDClient) config.ConfigFile {

	ipSets, _ := vdcGroup.getIPSets()
	dwfRules, _ := vdcGroup.getEdgeGatewayRules(vcdClient)
	edgeGatewayRules, _ := vdcGroup.getDFWRules(vcdClient)
	applicationPorts := vdcGroup.makeApplicationConfigList(vcdClient)

	vdcConfigFile := config.ConfigFile{
		VDC: config.VDCConfig{
			Name:             vdcGroup.Name,
			ApplicationPorts: applicationPorts,
			IPSets:           ipSets,
			EdgeGatewayFirewall: config.EdgeGatewayFirewallRulesetConfig{
				Rules: edgeGatewayRules,
			},
			DistributedFirewall: config.DistributedFirewallRulesetConfig{
				Rules: dwfRules,
			},
		},
	}

	return vdcConfigFile
}

func (vdcGroup *VDCGroup) getIPSets() ([]config.IPSet, error) {

	ipSetConfigList := make([]config.IPSet, 0)

	queryParams := url.Values{}
	queryParams.Add("filter", "ownerRef.id=="+url.PathEscape(vdcGroup.ID))
	queryParams.Add("sortAsc", "name")

	ipSets, err := vdcGroup.Org.GetAllNsxtFirewallGroups(queryParams, types.FirewallGroupTypeIpSet)
	if err != nil {
		return nil, err
	}

	for _, ipSet := range ipSets {
		// Get the details because GetAllNsxtFirewallGroups() does not fetch the details of the IP Adresses
		ipSetDetails, err := vdcGroup.Org.GetNsxtFirewallGroupById(ipSet.NsxtFirewallGroup.ID)
		if err != nil {
			return nil, err
		}
		ipSetConfig := config.IPSet{
			Name:        ipSetDetails.NsxtFirewallGroup.Name,
			Addresses:   ipSetDetails.NsxtFirewallGroup.IpAddresses,
			Description: ipSetDetails.NsxtFirewallGroup.Description,
		}

		ipSetConfigList = append(ipSetConfigList, ipSetConfig)
	}
	return ipSetConfigList, nil
}

func (vdcGroup *VDCGroup) getEdgeGatewayRules(vcdClient *govcd.VCDClient) ([]config.FirewallRuleConfig, error) {

	fwRules, err := vdcGroup.retrieveEdgeGatewayRules(vcdClient)
	if err != nil {
		return nil, err
	}

	return makeRuleConfigList(fwRules.UserDefinedRules), nil
}

func (vdcGroup *VDCGroup) getDFWRules(vcdClient *govcd.VCDClient) ([]config.FirewallRuleConfig, error) {
	fwRules, err := vdcGroup.retrieveDfwPolicies(vcdClient)
	if err != nil {
		return nil, err
	}

	return makeRuleConfigList(fwRules), nil
}

func (vdcGroup *VDCGroup) makeApplicationConfigList(vcdClient *govcd.VCDClient) []config.ApplicationPortProfileConfig {

	applicationPorts, _ := vdcGroup.getApplicationPorts(vcdClient)
	return makeApplicationConfigList(applicationPorts)
}

func makeApplicationConfigList(applications []*govcd.NsxtAppPortProfile) []config.ApplicationPortProfileConfig {
	appConfigList := make([]config.ApplicationPortProfileConfig, 0)

	for _, app := range applications {
		appConfig := config.ApplicationPortProfileConfig{
			Name:        app.NsxtAppPortProfile.Name,
			Ports:       toPortConfig(app.NsxtAppPortProfile.ApplicationPorts),
			Description: app.NsxtAppPortProfile.Description,
		}
		appConfigList = append(appConfigList, appConfig)
	}
	return appConfigList
}

func makeRuleConfigList(fwRules []types.NsxtFirewallRule) []config.FirewallRuleConfig {
	fwRuleConfigList := make([]config.FirewallRuleConfig, 0)

	for _, fwRule := range fwRules {
		fwRuleConfig := config.FirewallRuleConfig{
			Name:        fwRule.Name,
			Source:      openAPIReferenceToNamelist(fwRule.SourceFirewallGroups),
			Destination: openAPIReferenceToNamelist(fwRule.DestinationFirewallGroups),
			Application: openAPIReferenceToNamelist(fwRule.ApplicationPortProfiles),
		}
		fwRuleConfigList = append(fwRuleConfigList, fwRuleConfig)
	}
	return fwRuleConfigList
}

func openAPIReferenceToNamelist(refs types.OpenApiReferences) []string {
	names := make([]string, 0)
	for _, ref := range refs {
		names = append(names, ref.Name)
	}
	return names
}
