package vdc

import (
	"fmt"
	"strings"

	"github.com/vmware/go-vcloud-director/v2/govcd"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
)

func BuildRules(vcdClient *govcd.VCDClient, vdcConfig *config.VDCConfig, tenant string, rulePrefix string, dryRun bool) error {

	setDryRun(dryRun)

	org, err := vcdClient.GetOrgByNameOrId(tenant)
	if err != nil {
		return fmt.Errorf("organization %s not found: %w", tenant, err)
	}

	if err := Validate(vdcConfig, rulePrefix); err != nil {
		return err
	}

	vdc, err := org.GetVDCByName(vdcConfig.Name, false)
	if err != nil {
		return err
	}

	vdcGroup, err := getVDCGroup(vcdClient, org, vdc)
	if err != nil {
		return err
	}

	err = vdcGroup.manageApplicationPortconfig(vcdClient, vdcConfig, rulePrefix)
	if err != nil {
		return err
	}

	err = vdcGroup.manageIPSets(vdcConfig, rulePrefix)
	if err != nil {
		return err
	}

	err = vdcGroup.manageEdgeGatewayRules(vcdClient, vdcConfig, rulePrefix)
	if err != nil {
		return err
	}

	err = vdcGroup.dfwPolicies(vcdClient, vdcConfig, rulePrefix)
	if err != nil {
		return err
	}

	if err = vdcGroup.removeObsoleteIPSets(vdcConfig.IPSets, rulePrefix); err != nil {
		return err
	}

	return nil
}

func Validate(vdcConfig *config.VDCConfig, rulePrefix string) error {
	var sb strings.Builder

	for _, ipset := range vdcConfig.IPSets {
		if !strings.HasPrefix(ipset.Name, rulePrefix) {
			sb.WriteString(fmt.Sprintf("Invalid name for IPSet %q (prefix should be %q)\n", ipset.Name, rulePrefix))
		}
	}
	for _, rule := range vdcConfig.EdgeGatewayFirewall.Rules {
		if !strings.HasPrefix(rule.Name, rulePrefix) {
			sb.WriteString(fmt.Sprintf("Invalid name for Edgegateway FW rule %q (prefix should be %q)\n", rule.Name, rulePrefix))
		}
	}
	for _, rule := range vdcConfig.DistributedFirewall.Rules {
		if !strings.HasPrefix(rule.Name, rulePrefix) {
			sb.WriteString(fmt.Sprintf("Invalid name for Distributed FW rule %q (prefix should be %q)\n", rule.Name, rulePrefix))
		}
	}
	for _, app := range vdcConfig.ApplicationPorts {
		if !strings.HasPrefix(app.Name, rulePrefix) {
			sb.WriteString(fmt.Sprintf("Invalid name for Application Port config %q (prefix should be %q)\n", app.Name, rulePrefix))
		}
	}
	if sb.Len() > 0 {
		return fmt.Errorf(sb.String())
	}
	return nil
}
