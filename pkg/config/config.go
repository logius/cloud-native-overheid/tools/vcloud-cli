package config

import (
	"bytes"
	"io/ioutil"
	"path"
	"text/template"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

type ApplicationPortConfig struct {
	Protocol         string   `yaml:"protocol"`
	DestinationPorts []string `yaml:"destinationPorts,omitempty,flow"`
}

type ApplicationPortProfileConfig struct {
	Name        string `validate:"unique"`
	Ports       []ApplicationPortConfig
	Description string `yaml:",omitempty"`
}

type IPSet struct {
	Name        string   `validate:"unique"`
	Addresses   []string `yaml:",flow"`
	Description string   `yaml:",omitempty"`
}

type FirewallRuleConfig struct {
	Name        string   `validate:"unique"`
	Source      []string `yaml:",omitempty,flow"`
	Destination []string `yaml:",omitempty,flow"`
	Action      string   `yaml:",omitempty"`
	Application []string `yaml:",omitempty,flow"`
}

type EdgeGatewayFirewallRulesetConfig struct {
	Rules []FirewallRuleConfig `validate:"unique=Name"`
}

type DistributedFirewallRulesetConfig struct {
	Rules                 []FirewallRuleConfig `validate:"unique=Name"`
	CloneEdgeGatewayRules bool                 `yaml:"cloneEdgeGatewayRules"`
}

type VDCConfig struct {
	Name                string
	Template            string
	Zone                string                           `yaml:",omitempty"`
	ApplicationPorts    []ApplicationPortProfileConfig   `yaml:"applicationPorts" validate:"unique=Name"`
	IPSets              []IPSet                          `yaml:"ipSets" validate:"unique=Name"`
	EdgeGatewayFirewall EdgeGatewayFirewallRulesetConfig `yaml:"edgeGatewayFirewall"`
	DistributedFirewall DistributedFirewallRulesetConfig `yaml:"distributedFirewall"`
}

// ConfigFile configuration
type ConfigFile struct {
	VDC VDCConfig
}

// LoadConfig loads the VDC configuration from file
func LoadConfig(configFile string) (*VDCConfig, error) {
	validate := validator.New()
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}

	var cfg ConfigFile
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	if cfg.VDC.Template != "" {
		return renderConfigTemplate(&cfg, configFile)
	}

	err = validate.Struct(cfg.VDC)
	if err != nil {
		return nil, err
	}

	return &cfg.VDC, nil
}

func renderConfigTemplate(cfg *ConfigFile, configFile string) (*VDCConfig, error) {
	validate := validator.New()
	t, err := template.New(cfg.VDC.Template).ParseFiles(path.Dir(configFile) + "/" + cfg.VDC.Template)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	var templateData bytes.Buffer
	if err = t.Execute(&templateData, cfg.VDC); err != nil {
		return nil, err
	}
	var templatedCfg ConfigFile
	err = yaml.Unmarshal(templateData.Bytes(), &templatedCfg)
	if err != nil {
		return nil, errors.Errorf("could not parse the template config err = %v", err)
	}

	// Merge special rules, ipsets and ports into the template
	templatedCfg.VDC.ApplicationPorts = append(cfg.VDC.ApplicationPorts, templatedCfg.VDC.ApplicationPorts...)
	templatedCfg.VDC.IPSets = append(cfg.VDC.IPSets, templatedCfg.VDC.IPSets...)
	templatedCfg.VDC.EdgeGatewayFirewall.Rules = append(cfg.VDC.EdgeGatewayFirewall.Rules, templatedCfg.VDC.EdgeGatewayFirewall.Rules...)
	templatedCfg.VDC.DistributedFirewall.Rules = append(cfg.VDC.DistributedFirewall.Rules, templatedCfg.VDC.DistributedFirewall.Rules...)

	err = validate.Struct(templatedCfg)
	if err != nil {
		return nil, err
	}

	return &templatedCfg.VDC, nil
}

// LoadIPSetConfig loads the IPSet configuration from file
func LoadIPSetConfig(configFile string) ([]IPSet, error) {
	ipsets := make([]IPSet, 0)
	if configFile != "" {
		data, err := ioutil.ReadFile(configFile)
		if err != nil {
			return nil, errors.Errorf("Error reading YAML file: %s\n", err)
		}

		err = yaml.Unmarshal(data, &ipsets)
		if err != nil {
			return nil, errors.Errorf("Could not parse the config err = %v", err)
		}

	}
	return ipsets, nil
}
