package vcloud

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/hashicorp/go-version"
	"github.com/vmware/go-vcloud-director/v2/govcd"
)

// CreateClient creates a vCD client
// How to get a token: https://github.com/vmware/go-vcloud-director/blob/master/scripts/get_token.sh
func CreateClient(org string, apiUrl string, token string) (*govcd.VCDClient, error) {
	u, err := url.ParseRequestURI(apiUrl)
	if err != nil {
		return nil, fmt.Errorf("unable to pass url: %s", err)
	}

	vcdClient := govcd.NewVCDClient(*u, true)

	err = setVersion(vcdClient, apiUrl)
	if err != nil {
		return nil, err
	}

	userName := getEnv(fmt.Sprintf("VCLOUD_%s_USERNAME", org))
	password := getEnv(fmt.Sprintf("VCLOUD_%s_PASSWORD", org))
	if err := vcdClient.Authenticate(userName, password, org); err != nil {
		return nil, err
	}

	if userName != "" && password != "" {
		if err := vcdClient.Authenticate(userName, password, org); err != nil {
			return nil, err
		}
		return vcdClient, nil
	}

	// else login with token
	err = vcdClient.SetToken(org, govcd.AuthorizationHeader, token)
	if err != nil {
		return nil, fmt.Errorf("unable to set token: %w", err)
	}

	return vcdClient, nil
}

func getEnv(parameter string) string {
	parameter = strings.ToUpper(strings.ReplaceAll(parameter, "-", "_"))
	return os.Getenv(parameter)
}

func setVersion(vcdClient *govcd.VCDClient, url string) error {

	resp, err := http.Get(url + "/versions")
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type VersionInfo struct {
		Version string
	}
	type SupportedVersions struct {
		VersionInfo []VersionInfo
	}

	var supportedVersions SupportedVersions
	xml.Unmarshal(body, &supportedVersions)

	var lastVersion *version.Version
	for _, supportedVersion := range supportedVersions.VersionInfo {

		supportedVersionObject, _ := version.NewVersion(supportedVersion.Version)
		if lastVersion == nil || lastVersion.LessThan(supportedVersionObject) {
			lastVersion = supportedVersionObject
		}
	}

	vcdClient.Client.APIVersion = lastVersion.Original()
	log.Printf("VCloud API Version %q", vcdClient.Client.APIVersion)

	return nil
}
