package cmd

import (
	"github.com/spf13/cobra"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "vcloud",
		Short: "OPS tools for VCloud",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(NewConfigureVDCCommand())
	cmd.AddCommand(NewPrintCommand())
	cmd.AddCommand(ValidateConfigCommand())
}
