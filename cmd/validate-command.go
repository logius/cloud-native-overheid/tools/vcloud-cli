package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/vdc"
)

type validateFlags struct {
	config *string

	tenant     *string
	rulePrefix *string
	dryRun     *bool
}

func ValidateConfigCommand() *cobra.Command {

	flags := validateFlags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := validateConfig(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "validate-cfg",
		Short: "Validate Config",
		Long:  "This command validates the firewall rules.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to VDC configfile")
	flags.tenant = cmd.Flags().String("tenant", "", "VCloud Tenant")
	flags.dryRun = cmd.Flags().Bool("dry-run", false, "Dry run")
	flags.rulePrefix = cmd.Flags().String("rule-prefix", "", "Prefix for firewall rules ")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("tenant")
	cmd.MarkFlagRequired("rule-prefix")

	return cmd
}

func validateConfig(cmd *cobra.Command, flags *validateFlags) error {

	vdcConfig, err := config.LoadConfig(*flags.config)
	if err != nil {
		return err
	}
	log.Printf("VDC %q", vdcConfig.Name)

	if err != nil {
		return err
	}

	err = vdc.Validate(vdcConfig, *flags.rulePrefix)
	if err != nil {
		println("Invalid Config.")
		return err
	}
	println("Config Valid.")
	return nil
}
