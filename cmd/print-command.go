package cmd

import (
	"log"
	"os"
	"reflect"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/vdc"
	"gopkg.in/yaml.v2"
)

type VDCDiffMaps struct {
	EdgeGatewayRules map[string][]*VDCFirewallRule
	DfwRules         map[string][]*VDCFirewallRule
	IpSets           map[string][]*VDCIPSet
}

type VDCIPSet struct {
	name      string
	VDC       string
	Addresses []string `yaml:",flow"`
}

type VDCFirewallRule struct {
	name        string
	VDC         string
	Source      []string `yaml:",flow"`
	Destination []string `yaml:",flow"`
	Application []string `yaml:",flow,omitempty"`
}

// NewCommand creates a new command
func NewPrintCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := printTenant(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "print-tenant",
		Short: "Print rules in tenant",
		Long:  "This command prints IPSets and firewall rules in a tenant.",
	}

	flags.tenant = cmd.Flags().String("tenant", "", "VCloud Tenant")
	flags.az1VCloudAPIUrl = cmd.Flags().String("az1-vcloud-api-url", "", "AZ1 VCloud API URL")
	flags.az2VCloudAPIUrl = cmd.Flags().String("az2-vcloud-api-url", "", "AZ2 VCloud API URL")
	flags.rulePrefix = cmd.Flags().String("rule-prefix", "", "Prefix for firewall rules ")

	cmd.MarkFlagRequired("az1-vcloud-api-url")
	cmd.MarkFlagRequired("az2-vcloud-api-url")
	cmd.MarkFlagRequired("tenant")
	cmd.MarkFlagRequired("rule-prefix")

	return cmd
}

func printTenant(cmd *cobra.Command, flags *flags) error {
	var configFilesAZ1 []config.ConfigFile
	var configFilesAZ2 []config.ConfigFile
	var err error

	tenant := *flags.tenant
	rulePrefix := *flags.rulePrefix

	hasUserNamePassword := os.Getenv("VCLOUD_USERNAME") != "" && os.Getenv("VCLOUD_PASSWORD") != ""

	if hasUserNamePassword || len(os.Getenv("AZ1_TOKEN")) > 0 {
		apiURL := *flags.az1VCloudAPIUrl
		zone := "az1"
		log.Printf("Analyze rules in tenant %q in %q", tenant, zone)

		configFilesAZ1, err = vdc.GetRulesForTenant(tenant, apiURL, zone, os.Getenv("AZ1_TOKEN"), rulePrefix)
		if err != nil {
			return err
		}
	}
	if hasUserNamePassword || len(os.Getenv("AZ2_TOKEN")) > 0 {
		apiURL := *flags.az2VCloudAPIUrl
		zone := "az2"
		log.Printf("Analyze rules in tenant %q in %q", tenant, zone)

		configFilesAZ2, err = vdc.GetRulesForTenant(tenant, apiURL, zone, os.Getenv("AZ2_TOKEN"), rulePrefix)
		if err != nil {
			return err
		}
	}

	os.Mkdir("tenant", os.ModePerm)

	writeYAMl(configFilesAZ1, tenant, "az1", rulePrefix, true)
	writeYAMl(configFilesAZ1, tenant, "az1", rulePrefix, false)

	writeYAMl(configFilesAZ2, tenant, "az2", rulePrefix, true)
	writeYAMl(configFilesAZ2, tenant, "az2", rulePrefix, false)

	return nil
}

func writeYAMl(configFiles []config.ConfigFile, tenant string, zone string, rulePrefix string, include bool) {
	vdcConfigFilesFiltered := filterRules(configFiles, rulePrefix, include)
	for _, configFile := range vdcConfigFilesFiltered {
		yamlData, _ := yaml.Marshal(configFile)
		fileName := strings.Replace(configFile.VDC.Name, tenant+"-", "", 1)
		if include {
			fileName += "-" + strings.ReplaceAll(rulePrefix, "-", "")
		} else {
			fileName += "-default"
		}
		os.WriteFile("tenant/"+fileName+".yaml", yamlData, 0644)
	}

	diffMap := createDiffMap(vdcConfigFilesFiltered)
	yamlData, _ := yaml.Marshal(diffMap)

	fileName := zone
	if include {
		fileName += "-" + strings.ReplaceAll(rulePrefix, "-", "")
	} else {
		fileName += "-default"
	}
	readme := `#
# Comparison of rules in a tenant. 
# IPSet and Firewall rules are compared between VDC groups. 
# The rules are printed if there was a difference found. Otherwise and empty array [] is shown. 
#
`
	yamlData = append([]byte(readme), yamlData...)

	os.WriteFile("tenant/"+fileName+".diff.yaml", yamlData, 0644)
}

func createDiffMap(configFiles []config.ConfigFile) VDCDiffMaps {
	vdcDiffMaps := VDCDiffMaps{}
	vdcDiffMaps.IpSets = checkIPSets(configFiles)
	vdcDiffMaps.DfwRules = checkFWRules(dfwRulesToMap(configFiles), configFiles)
	vdcDiffMaps.EdgeGatewayRules = checkFWRules(edgeRulesToMap(configFiles), configFiles)
	return vdcDiffMaps
}

func checkIPSets(configFiles []config.ConfigFile) map[string][]*VDCIPSet {
	ipSetsMap := make(map[string][]*VDCIPSet)

	for _, configFile := range configFiles {
		for _, ipSet := range configFile.VDC.IPSets {
			ipSets := ipSetsMap[ipSet.Name]
			vdcIPSet := VDCIPSet{
				name:      ipSet.Name,
				VDC:       configFile.VDC.Name,
				Addresses: ipSet.Addresses,
			}
			ipSetsMap[ipSet.Name] = append(ipSets, &vdcIPSet)
		}
	}

	keys := make([]string, 0, len(ipSetsMap))
	for k := range ipSetsMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	ipSetsDiffMap := make(map[string][]*VDCIPSet)
	for _, k := range keys {
		if ipSetsDiffer(ipSetsMap[k], len(configFiles)) {
			ipSetsDiffMap[k] = ipSetsMap[k]
		} else {
			ipSetsDiffMap[k] = make([]*VDCIPSet, 0)
		}
	}
	return ipSetsDiffMap
}

func dfwRulesToMap(configFiles []config.ConfigFile) map[string][]*VDCFirewallRule {
	fwRulesMap := make(map[string][]*VDCFirewallRule)

	for _, configFile := range configFiles {
		for _, fwRule := range configFile.VDC.DistributedFirewall.Rules {
			fwRules := fwRulesMap[fwRule.Name]
			vdcFWRule := VDCFirewallRule{
				name:        fwRule.Name,
				VDC:         configFile.VDC.Name,
				Source:      fwRule.Source,
				Destination: fwRule.Destination,
				Application: fwRule.Application,
			}
			fwRulesMap[fwRule.Name] = append(fwRules, &vdcFWRule)
		}
	}
	return fwRulesMap
}

func edgeRulesToMap(configFiles []config.ConfigFile) map[string][]*VDCFirewallRule {
	fwRulesMap := make(map[string][]*VDCFirewallRule)

	for _, configFile := range configFiles {
		for _, fwRule := range configFile.VDC.EdgeGatewayFirewall.Rules {
			fwRules := fwRulesMap[fwRule.Name]
			vdcFWRule := VDCFirewallRule{
				name:        fwRule.Name,
				VDC:         configFile.VDC.Name,
				Source:      fwRule.Source,
				Destination: fwRule.Destination,
				Application: fwRule.Application,
			}
			fwRulesMap[fwRule.Name] = append(fwRules, &vdcFWRule)
		}
	}
	return fwRulesMap
}

func checkFWRules(fwRulesMap map[string][]*VDCFirewallRule, configFiles []config.ConfigFile) map[string][]*VDCFirewallRule {

	keys := make([]string, 0, len(fwRulesMap))
	for k := range fwRulesMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	fwRuleDiffsMap := make(map[string][]*VDCFirewallRule)
	for _, k := range keys {
		if firewallsDiffer(fwRulesMap[k], len(configFiles)) {
			fwRuleDiffsMap[k] = fwRulesMap[k]
		} else {
			fwRuleDiffsMap[k] = make([]*VDCFirewallRule, 0) // show as empty
		}
	}

	return fwRuleDiffsMap
}

func filterRules(configFiles []config.ConfigFile, rulePrefix string, include bool) []config.ConfigFile {
	configFilesFiltered := make([]config.ConfigFile, 0)

	for _, configFile := range configFiles {
		configFileFiltered := configFile
		rules := make([]config.FirewallRuleConfig, 0)
		for _, rule := range configFile.VDC.DistributedFirewall.Rules {
			if strings.HasPrefix(rule.Name, rulePrefix) == include {
				rules = append(rules, rule)
			}
		}
		configFileFiltered.VDC.DistributedFirewall.Rules = rules

		rules = make([]config.FirewallRuleConfig, 0)
		for _, rule := range configFile.VDC.EdgeGatewayFirewall.Rules {
			if strings.HasPrefix(rule.Name, rulePrefix) == include {
				rules = append(rules, rule)
			}
		}
		configFileFiltered.VDC.EdgeGatewayFirewall.Rules = rules

		ipSets := make([]config.IPSet, 0)
		for _, ipSet := range configFile.VDC.IPSets {
			if strings.HasPrefix(ipSet.Name, rulePrefix) == include {
				ipSets = append(ipSets, ipSet)
			}
		}
		configFileFiltered.VDC.IPSets = ipSets

		applicationPorts := make([]config.ApplicationPortProfileConfig, 0)
		for _, applicationPort := range configFile.VDC.ApplicationPorts {
			if strings.HasPrefix(applicationPort.Name, rulePrefix) == include {
				applicationPorts = append(applicationPorts, applicationPort)
			}
		}
		configFileFiltered.VDC.ApplicationPorts = applicationPorts

		configFilesFiltered = append(configFilesFiltered, configFileFiltered)
	}
	return configFilesFiltered
}

func firewallsDiffer(fwRules []*VDCFirewallRule, expectedSize int) bool {
	if len(fwRules) != expectedSize {
		return true
	}
	for _, fwRule1 := range fwRules {
		for _, fwRule2 := range fwRules {
			if fwRule1.name != fwRule2.name {
				if !reflect.DeepEqual(fwRule1.Source, fwRule2.Source) {
					return true
				}
				if !reflect.DeepEqual(fwRule1.Destination, fwRule2.Destination) {
					return true
				}
				if !reflect.DeepEqual(fwRule1.Application, fwRule2.Application) {
					return true
				}
			}
		}
	}

	return false
}

func ipSetsDiffer(ipSets []*VDCIPSet, expectedSize int) bool {
	if len(ipSets) != expectedSize {
		return true
	}
	for _, ipSet1 := range ipSets {
		for _, ipSet2 := range ipSets {
			if ipSet1.name != ipSet2.name && !reflect.DeepEqual(ipSet1.Addresses, ipSet2.Addresses) {
				return true
			}
		}
	}

	return false
}
