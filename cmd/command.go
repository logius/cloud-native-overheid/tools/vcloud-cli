package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/config"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/vcloud"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/pkg/vdc"
)

type flags struct {
	config *string

	az1VCloudAPIUrl *string
	az2VCloudAPIUrl *string
	tenant          *string
	rulePrefix      *string
	dryRun          *bool
}

func NewConfigureVDCCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureVDC(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-vdc",
		Short: "Configure VDC",
		Long:  "This command configures firewall rules in VDC.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to VDC configfile")
	flags.tenant = cmd.Flags().String("tenant", "", "VCloud Tenant")
	flags.az1VCloudAPIUrl = cmd.Flags().String("az1-vcloud-api-url", "", "AZ1 VCloud API URL")
	flags.az2VCloudAPIUrl = cmd.Flags().String("az2-vcloud-api-url", "", "AZ2 VCloud API URL")
	flags.rulePrefix = cmd.Flags().String("rule-prefix", "", "Prefix for firewall rules ")
	flags.dryRun = cmd.Flags().Bool("dry-run", false, "Dry run")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("az1-vcloud-api-url")
	cmd.MarkFlagRequired("az2-vcloud-api-url")
	cmd.MarkFlagRequired("tenant")
	cmd.MarkFlagRequired("rule-prefix")

	return cmd
}

func configureVDC(cmd *cobra.Command, flags *flags) error {

	vdcConfig, err := config.LoadConfig(*flags.config)
	if err != nil {
		return err
	}
	log.Printf("VDC %q", vdcConfig.Name)

	if err != nil {
		return err
	}
	if vdcConfig.DistributedFirewall.CloneEdgeGatewayRules {
		log.Printf("Distributed FW rules will be cloned from EdgeGateway")
		vdcConfig.DistributedFirewall.Rules = vdcConfig.EdgeGatewayFirewall.Rules
	}

	var apiURL string
	var token string
	if vdcConfig.Zone == "az1" {
		apiURL = *flags.az1VCloudAPIUrl
		token = os.Getenv("AZ1_TOKEN")
	} else if vdcConfig.Zone == "az2" {
		apiURL = *flags.az2VCloudAPIUrl
		token = os.Getenv("AZ2_TOKEN")
	}
	vcdClient, err := vcloud.CreateClient(*flags.tenant, apiURL, token)
	if err != nil {
		return err
	}

	return vdc.BuildRules(vcdClient, vdcConfig, *flags.tenant, *flags.rulePrefix, *flags.dryRun)
}
