package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/vcloud-cli/cmd"
)

func main() {
	rootCmd := &cobra.Command{}
	// We have to do this otherwise we get the Usage printed at every error which is not helpful.
	rootCmd.SilenceUsage = true
	// We already print the error, we don't need a secondary print
	rootCmd.SilenceErrors = true

	cmd.AddSubCommands(rootCmd)

	if err := rootCmd.Execute(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}
